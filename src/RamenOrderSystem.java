import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class RamenOrderSystem {
    private JPanel root;
    private JButton saburou;
    private JButton syouyu;
    private JButton shio;
    private JButton tonkotsu;
    private JButton mazesoba;
    private JButton tsukemen;
    private JLabel foodName1;
    private JLabel foodName2;
    private JLabel foodName3;
    private JLabel foodName4;
    private JLabel foodName5;
    private JLabel foodName6;
    private JTextPane Itemlist;
    private JButton CheckOut;
    private JLabel TotalYen;
    private JButton cansel;
    int total=0,yen=0;
    String size;
    int price(String ramen){
        if(ramen == "Saburou") {
            return 880;
        }
        else if(ramen == "Syouyu") {
            return 550;
        }
        else if(ramen == "Shio") {
            return 660;
        }
        else if(ramen == "Tonkotsu") {
            return 770;
        }
        else if(ramen == "Mazesoba") {
            return 800;
        }
        else if(ramen == "Tsukemen") {
            return 990;
        }
        return 0;
}

    void order(String ramen) {
        String noodles[] = {"+100g(+50yen)","-100g(-50yen)","No change"};

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + ramen + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            int amount = JOptionPane.showOptionDialog(null,
                    "Would you change the amount of noodles?",
                    "Amount of noodles",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    noodles,
                    noodles[0]);

            if (amount == 0) {
                amount += 50;
                size = "Big ";
            } else if (amount == -1) {
                amount += 1;
                size = "";
            } else if(amount == 1) {
                amount -= 51;
                size = "Small ";
            }else if (amount == 2) {
                amount -= 2;
                size = "";
            }

            yen = price(ramen) + amount;
            total += yen;
            TotalYen.setText("Total     " + total + " yen");
            String currentText = Itemlist.getText();
            Itemlist.setText(currentText + size + ramen + " " + yen + "yen" + "\n");
            JOptionPane.showMessageDialog(null, "Order for " + size + ramen + " received.");

        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("RamenOrderSystem");
        frame.setContentPane(new RamenOrderSystem().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(1550,800);
        frame.setVisible(true);
    }

    public RamenOrderSystem() {

        saburou.setIcon(new ImageIcon( this.getClass().getResource("./images./saburou.jpg")));
        syouyu.setIcon(new ImageIcon( this.getClass().getResource("./images./syouyu.jpg")));
        shio.setIcon(new ImageIcon( this.getClass().getResource("./images./shio.jpg")));
        tonkotsu.setIcon(new ImageIcon( this.getClass().getResource("./images./tonkotsu.jpg")));
        mazesoba.setIcon(new ImageIcon( this.getClass().getResource("./images./mazesoba.jpg")));
        tsukemen.setIcon(new ImageIcon( this.getClass().getResource("./images./tsukemen.jpg")));



        saburou.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Saburou");
            }

        });
        syouyu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Syouyu");
            }
        });
        shio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shio");
            }
        });
        tonkotsu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tonkotsu");
            }
        });
        mazesoba.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mazesoba");
            }
        });
        tsukemen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tsukemen");
            }
        });
        CheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to issue ticket?",
                        "Issuing ticket Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ total + " yen.");
                    Itemlist.setText(" ");
                    total = 0;
                    TotalYen.setText("Total     "+total+" yen");
                }
            }
        });
        cansel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to CANSEL?",
                        "CANSEL",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    Itemlist.setText(" ");
                    total = 0;
                    TotalYen.setText("Total     " + total + " yen");
                }
            }
        });
    }
}
